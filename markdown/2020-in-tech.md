---
title: 2020 in technology
author: rnxpyke
published: 2021-01-21 22:17:00
---

# This year in tech

## Worst Winner Award

The award for the worst winner of this year goes to:
>  Zoom Video Communications, Inc.

Many businesses have been hit hard by COVID-19 and reacted by reducing physical contact as much as possible.
A big portion of universities and schools that can't do without large meetings have been looking for an online alternative.
There already where some popular tools that catered to that niche, but software like Skype began to suck after being bought by Microsoft, Discord isn't business-y enough and only supports up to 25 video feeds at once, and Google Hangouts is just like Google+, namely just not popular enough.

Instead, the corporate need for big video calls and the network effect made Zoom the go to tool for handling online communication in 2020. This is rather unfortunate, because Zoom has had a row of bad business practices and insecure implementations that got them pretty bad press.

Other than „Zoombombing“ and weak implementation of their client and protocol that gave hackers access to the cameras of users, Zoom has been 'a close business partner' of Facebook, if you know what I mean. Zoom claimed[^zoom-end-to-end] to have end to end encryption of their services earlier this year, but the feature only came into 'technical preview' later in October.[^zoom-end-to-end-impl] And Zoom isn't just bad on a technical standpoint. Their feature that the meetings host gets notified about the (in)-activity of the participants only got removed one month after the EFFs public complaint about privacy concerns.[^zoom-eff]

With the growth of zooms user base from about 10 million in December 2019 to over 300 million in April 2020 and its market valuation rising from $16 billion in January 2020 to $40 billion in march 2020 it is clearly the worst winner of 2020. ([Source](https://www.businessofapps.com/data/zoom-statistics/)) 
[^zoom-end-to-end]: [The intercept on zoom encryption](https://theintercept.com/2020/03/31/zoom-meeting-encryption/)

[^zoom-end-to-end-impl]: [Zoom blog on e2e rollout](https://blog.zoom.us/zoom-rolling-out-end-to-end-encryption-offering/)

[^zoom-eff]: [EFF on zooms attention tracking](https://www.eff.org/deeplinks/2020/03/what-you-should-know-about-online-tools-during-covid-19-crisis) 



## .org stays in 'public' hand, for now

Pretty much everyone who spends his or hear time on the internet knows about domain names, and most of us appreciate a good domain hack.[^domain-hack] But what most people don't know is who exactly has control over how these domain names get distributed.

[^domain-hack]: [Wikipedia: Domain Hack](https://en.wikipedia.org/wiki/Domain_hack)

The entity has the important job of overseeing the domain name system has an appropriately funny name. The Internet Corporation for Assigned Names and Numbers, or ICANN, in short, is a non profit organisation in Los Angeles, California, has the ultimate authority over DNS and the distribution of IP addresses, although they mostly delegate the control of DNS by using the hierarchical nature of domains and giving other organizations control over so called Top-Level-Domains.

`.org` is such a top level domain, with a pretty interesting history. Besides being having over 10 million domains registered, according to ICANNs wiki[^orgwiki] it was originally created alongside `.com` `.edu` and `.gov` by the Network Working Group in 1984. It was managed by SRI[^sri] Internationals Network Information Center, until it changed ownership to Network Solutions[^network-solutions] in agreement with the National Science Foundation[^NSF].

[^orgwiki]: [ICANN .org wiki page](https://icannwiki.org/.org)
[^network-solutions]: [Network Solutions](https://www.networksolutions.com/)
[^NSF]: [National Science Foundation](https://www.nsf.gov/)

In 1998 The U.S. Department of Commerce[^USDOP] issued a white paper in response to instructions of 2002.
This agreement didn't hold very long, because VeriSign[^veriSign] acquired the majority share of Network Solutions at a price of $20 billion.
Because VeriSign also had control over the popular `.com` and `.net` TLDs ICANN received multiple proposals to change management over the `.org` domain. They evaluated all and recommended three of them to the Department of Commerce, which approved the Public Interest Registry[^pir] to take over the management of `.org`, which still manages the TLD to this day.

[^USDOP]: [U.S. Department of Commerce](https://www.commerce.gov/)
[^sri]: [SRI International](https://www.sri.com/)
[^veriSign]: [VeriSign](https://www.verisign.com/)
[^pir]: [Public Interest Registry](https://thenew.org/org-people/about-pir/)

But enough with the history, and onto the recent news:

Fadi Chehadé[^ican-fade] was President and CEO of ICANN from 2012 to 2016. Now he is Co-CEO at Ethos Capital[^ethos-capital], a private equity investment firm which tried to acquire the Public Interest Registry, and therefore `.org`, for $1.135 billion.

The PIR proposed the change of the TLD to ICANN, after its parent organization, the ISOC[^isoc] reached agreement with Ethos Capital that PIR would be acquired by Ethos Capital, thereby converting from a non-profit to a for-profit limited liability company.

[^ican-fade]: [ICANN about old CEO Chehadé](https://www.icann.org/resources/pages/fadi-chehade-2016-07-05-en)
[^ethos-capital]: [Ethos Capital - Fadi Chehadé](https://www.ethoscapital.com/bio/fadichehade)
[^isoc]: [ISOC: Internet Society](https://www.internetsociety.org/)

Although Ethos Capital and the ISOC seem to have no problem with such a deal, the ICANN explains in its press release[^icann-press] that public interest is better served when the change of hands of the `.org` TLD doesn't happen. Hats of to ICANN!

[^icann-press]: [ICANN on withholding consent of transfer of .org TLD](https://www.icann.org/news/blog/icann-board-withholds-consent-for-a-change-of-control-of-the-public-interest-registry-pir)

## Lawmakers and the beef with encryption

Lawmakers still want to put backdoors in encryption. For technologically adapt people, it is clear that banning encryption or demanding access for state level actors is deemed to fail from the start, be it by failing to acquire enough votes in a democracy or, should it eventually happen, by disrupting the free (as in freedom) life we know and cherish, and falling back to totalitarian control, worse than even Orwell could have dreamed about.

Okay, maybe that's putting it on a bit to thick, but real talk, banning encryption is really bad, nobody should do it. I'm choosing the word 'should' on purpose though, because it seems that you should expect that people will try by now. The United States proposed the EARN IT Bill, which tries to gain control over service provides, supposedly to lower sexual exploitations of minors happening online. Service provides would have to certify compliance with „best practices” to fight online exploitation. The „best practices” would be developed by a commission with the attorney general at its head. Should service provides choose to not comply, they could be stripped of their Section 230[^section-230] immunity, which states that service provider is not accountable for the actions of their users.

And with Attorney General William Barr, who is known for hostility towards encryption[^barr], it is to be expected that the above mentioned „best practices” would include abolishment of end-to-end encryption.

[^section-230]: [Section 230 Communications Decency Act](https://www.law.cornell.edu/uscode/text/47/230)
[^barr]: [Barr's hostility towards encryption](https://www.wsj.com/articles/barrs-encryption-push-is-decades-in-the-making-but-troubles-some-at-fbi-11579257002)

Even worse, they just proposed the „Lawful Access to Encrypted Data Act“. This is a pretty obvious take against encryption. One can speculate that it should make EARN IT seem legitimate in comparison.
For a perspective a bit more thought out than mine, check out the CiS [Cyberlaw Blog](https://cyberlaw.stanford.edu/blog/2020/06/there%E2%80%99s-now-even-worse-anti-encryption-bill-earn-it-doesn%E2%80%99t-make-earn-it-bill-ok).

But not just the USA have something against privacy online, the EU had to make a move against encryption themselves. The „EU Council Draft Declaration against Encryption“[^eu-against-encryption] is a fairly short document, that tries move towards solutions for „security despite encryption“. Although the draft recognises the need for strong encryption, it argues that it is to easy for criminals to use end-to-end encryption through normal messaging apps, which makes „access to electronic evidence extremely challenging“.

Therefore they conclude that they need to start a „discussion“ with the tech industry to develop strong encryption that leaves law enforcement the possibility to „access data in a lawful and targeted manner, [..] while upholding cyber security“.

The draft also states the following:
>„There is a need for a regulatory framework that safeguards fundamental rights and the advantages of end-to-end encryption and which allows law enforcement and judicial authorities to carry out their tasks.“

Clearly, this is asking for the impossible. There can't be a safe end-to-end encryption scheme which is also accessible by a third party by definition. The only way to access the content of secure end-to-end encrypted data is to get the encryption key of the end user. This would necessitate that the service provider or law enforcement has to store these keys for later access, for all users, whether they are suspected of a crime or not.

This approach makes messaging and other end-to-end encrypted platforms insecure for everyone. It would only take one leak of session keys to access all users texts. And such incidents aren't unheard of.

And even if the „tech industry” magically discovers an encryption scheme which ensures security for the „normal” user and lets law enforcement access the data of „criminals“, this wouldn't make things better. Platforms would need to adopt these new schemes, which would mean that regular end-to-end encryption is likely to become outlawed itself.

This wouldn't help anyone. Organized criminals still could use end-to-end encryption. They don't care if their encryption is „illegal“, they are criminals. Now law enforcement can only access the data of normal citizens and low profile criminals. They haven't gained anything in regards to stopping high profile crime.

I'll just leave this here:
> **You can't outlaw encryption for the baddies.**

Why do you want to outlaw encryption for us?


[^eu-against-encryption]: [EU Council Draft Declaration against Encryption](https://www.statewatch.org/media/1434/eu-council-draft-declaration-against-encryption-12143-20.pdf)

## RIAA vs youtube-dl

The Recording Industry Association of America, or RIAA for short, had a pretty surprising notice for GitHub. They want the repository of `youtube-dl` to be taken down.[^riaa-takedown] `youtube-dl` is a popular command line software that makes it easy do download videos from popular websites, like YouTube.

Apparently, the Readme of youtube-dl featured some examples that contained links to songs with US copyright. This lead RIAA to believe that youtube-dl is a tool to circumvent the YouTube „rolling cipher“ which is a effective technical measure to implement digital rights according to German law.

Why exactly they think that some German court is of matter for a US based video hosting platform and a tool hosted on GitHub, also a US based company, I don't know. 

GitHub's default policy is to take down the repository until the other party has made a counterclaim of some sorts, so the `youtube-dl` repository went down. The community responded by uploading even more copies to GitHub and other hosting platforms. One user even used a quirk of the GitHub servers to put the `youtube-dl` source code into the DMCA repository owned by GitHub.[^dmca-dl]

[^riaa-takedown]: [RIAA: youtube-dl DMCA notice](https://github.com/github/dmca/blob/master/2020/10/2020-10-23-RIAA.md)
[^dmca-dl]: [GitHub DMCA Repo: youtube-dl zip download](https://github.com/github/dmca/archive/416da574ec0df3388f652e44f7fe71b1e3a4701f.zip)

In the end, Microsoft, the owner of GitHub, sited with the `youtube-dl` authors and put the repository back online. You can read about it from the authors here: [Youtube-dl is back](https://github.com/ytdl-org/youtube-dl/issues/27013).

## Mozilla layoffs

Mozilla is one of the internet companies that people looked up to in hope to stay a viable and acceptable competitor to the big players of the time. It's web browser, Firefox had up to 30% of the market share at it's peak.

But currently, their market share is on decline, as is their revenue. In Mozilla's Blog[^mozilla-blog], the CEO Mitchell Baker announces the new business direction of his company, as well as the layoffs of about 250 employees, or 25% of their workforce.

[^mozilla-blog]: [Mozilla Blog on layoffs and restructuring](https://blog.mozilla.org/blog/2020/08/11/changing-world-changing-mozilla/)

With the new "focus on product" Mozilla seeks out to make money on their own terms. Currently most of the revenue comes from deals with Google and other big search engines in the Chinese markets. These players bay huge sums to be featured as the default search engine in Mozilla's browser. Although being payed by your competitor isn't the best business strategy, there might be a point where Firefox isn't a major player anymore, which means that Mozilla won't even get that revenue stream.

When looking at it from this perspective, it seems weird that Baker takes focus away from the browser that's slowly loosing it's market share, while getting enormous raises[^mozilla-money] at the same time.

[^mozilla-money]: [Mozilla Chair pay vs Market share](http://calpaterson.com/mozilla.html)

But at least for the 250 engineers that were laid off there is hope. Different companies are taking the opportunity to get their hands on the creators of Firefox and Mozilla's maturing research project, the Rust programming language. At [mozillalifeboat.com](https://mozillalifeboat.com) ex-Mozilla engineers have the opportunity to find new companies looking for excellent developers.

## Biggest fail of the year

Google easily fulfills the criteria for the biggest fail of the year. Not only did they had some bad press by accepting an ultimatum of an AI ethics researcher[^google-ethics], they also had multiple outages.

The first outage took down the login part of Googles infrastructure for approximately 2 hours. At the time it felt like Tom Scott's horror scenario[^google-scott] came true, but apparently, no private data has been leaked. Closely following this event, Google's mail server showed severe misbehaviour. It replied that the recipient account didn't exist for many legal mails. You can read the outage reports here.[^google-outage]

[^google-ethics]: [Google and AI Researcher](https://www.businessinsider.com/timnit-gebru-ethical-ai-fired-google-2020-12?r=DE&IR=T)
[^google-scott]: [Tom Scott: Single Point of Failure](https://www.youtube.com/watch?v=y4GB_NDU43Q)
[^google-outage]: [Google Outage Report](https://status.cloud.google.com/incident/zall/20013#20013004)


## AMD continues to crush Intel on consumer processor hardware
['nuf said](https://www.youtube.com/embed/iuiO6rqYV4o)

## Advances in AI

Since 2015, where DeepMind's AlphaGo beat a professional Go player for the first time, they have build up an impressive resume of doing the impossible using neural networks. This includes AlhpaGo Master, which played Go online, and continued with AlphaZero in 2017, which generalized the approach of learning by self play to chess and shogi as well. And in 2019 they even managed to compete in the hidden information real-time strategy game Starcraft on a competitive level.

This year, they focused on the CASP[^casp], a yearly challenge concerning the Critical Assessment of protein Structure Prediction. At this point it isn't even that surprising anymore, that they managed an overall prediction accuracy of over 90% with their model, AlphaFold 2[^deepmind-alphafold]. This is basically the threshold at which protein folding is considered solved, because of potential measurement errors of experimental methods for determining protein structure, which is used as training data and ground truth for the CASP challenge.

[^casp]: [CASP Challenge](https://predictioncenter.org/)
[^deepmind-alphafold]: [DeepMind's Blog Post CASP 2020](https://deepmind.com/blog/article/alphafold-a-solution-to-a-50-year-old-grand-challenge-in-biology)

And as always, DeepMind has a great [video recap](https://www.youtube.com/watch?v=gg7WjuFs8F4) on their research, go check it out!

## Apple and its love-hate relationship with consumers

Last but not least, there is some news about apple this year that's actually consumer friendly!

Apple announced that they would implement a new TrackingTransparency Framework[^tracking-transparency], which gives the user a native tracking prompt. If I understood correctly, they only give the devices unique tracking identifier should the user opt in. Before, apps could access such an identifier without the user noticing.

Facebook on the other hand fears drops in tracking revenue and posted newspaper adds against apples new Tracking Framework. With Titles like „We're standing up to Apple for small businesses everywhere“ and „Apple vs. the free internet“ they want to spark opposition of users against their own privacy.

And with no pity for Facebook my summary of 2020 in technology comes to it's end.

Let's all hope that 2021 will be a better year.

[^tracking-transparency]: [Apple AppTrackingTransparency Framework](https://developer.apple.com/documentation/apptrackingtransparency)
[^facebook-fail]: [Facebook criticising apple](https://thebigtech.substack.com/p/facebook-criticising-apples-ios-14)

