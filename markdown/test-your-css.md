---
title: Test your CSS!
author: rnxpyke
published: 2019-03-31 14:30:00
---

# Test your CSS!

## Text Formatting

Yay !
Here we are.
Here is something *important*.
There is also something **strong**.
We are, of course, in a paragraph.
And it contains an inline quotation :
Richard M. Stallman said
"La cuisine indienne favorise les plats curryeux." [^1]

But... What about blockquotes!? Let's test:
> There are some people who live in a dream world,
> and there are some who face reality;
> and then tere are those who turn one into the other.
> — Douglas Everett

That was a fine quote. Now, go on!

## Emphasis

Emphasis, aka italics, with *asterisks* or _underscores_.
Strong emphasis, aka bold, with **asterisks** or __underscores__.
Combined emphasis with **asterisks and _underscores_**.
Strikethrough uses two tildes. ~~Scratch this.~~

## Long texts

Lorem ipsum dolor sit amet, consectetur adispiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna alqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ae commodo conequat. Duis aute irure dolor in e...

### Even Longer Texts

Lorem ipsum dolor sit amet, consectetur adispiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna alqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ae commodo conequat. Duis aute irure dolor in e...

### Some more Texts

Lorem ipsum dolor sit amet, consectetur adispiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna alqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ae commodo conequat. Duis aute irure dolor in e...

## Code Listings

this is some inline `code`

    this is code
    and another line

This is some python block code:

```python
def main():
    print("Hello World!")
```

## Tables

Colons can be used to align columns.

Colons can be used to align columns.

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

There must be at least 3 dashes separating each header cell. The outer pipes (|) are optional, and you don't need to make the raw Markdown line up prettily. You can also use inline Markdown.

Markdown | Less | Pretty
--- | --- | ---
*Still* | `renders` | **nicely**
1 | 2 | 3

## Nestings

This is a test for different nesting levels.

### H3

This line is on nesting level 3.

#### H4

This Block contains two H5 blocks.

##### H5

some text

##### H5 again

###### H6

some normal text in a deeply nested structure

[^1]: This is a footnote test
