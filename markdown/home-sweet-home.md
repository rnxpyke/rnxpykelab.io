---
title: Home Sweet Home
author: rnxpyke
published: 2019-12-01 11:00:00
---

# Home Sweet Home

As a linux power user your home is an extension of your mind. Your home directory, that is. I personally am *in the zone* when I can focus coding, and a cluttered home directory is a focus killer. The only problem is:
> Every software project out there thinks that **it** has the right to save configuration in my home directory.

Spoiler: *You don't.*
And there is a perfectly reasonable standard to put your stuff into. It's the XDG Base Directory Specification, and you should [read it](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html). It won't take that long, I'll wait 'til you are finished. In short, this specification is the reason that some programs put their configuration files in the .config directory. To be precise, it only specifies how the environment variables referring to those directories should be interpreted, but in effect it *should* move random dotfiles into meaningful directories.

To illustrate, the dotfiles in my current home directory look something like this:

    .config/    .local/    .cache/
    .vim/       .ssh/      .ghc/
    .pam_environment       .profile

The first three directories are from the XDG specification. They contain configuration, user specific data and temporary data respectively. The next three are some offenders who hardcoded their config dirs or are annoying to set up correctly. The last two files are responsible for keeping all the other applications with bad defaults out of my home directory.

I set the XDG environment variables explicitly and then point things like the bash and python history, that can in turn be relocated through environment variables, into their correct places in the config and data directories. For reference, see the [XDG](https://wiki.archlinux.org/index.php/XDG_Base_Directory) Arch Wiki entry.

Furthermore, my home directory is read-only. This way nothing can *accidentally* create new dotfiles. Sometimes I install a new package and I can't run it because of that. But that's not really an issue, because then I can look into fixing the dotfiles manually or decide to not use the software after all.

It's worth mentioning Thunderbird as an example. Thunderbird not only creates a `.thunderbird` directory, it also depends on the Mozilla package which uses `.mozilla` **and** it stores some data in the *correct* `.local/share/thunderbird`. Therefore I kicked it from my system.

It's not a perfect system by any means, but it works for me.
