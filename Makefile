SOURCES := $(shell cd markdown; find -mindepth 1 -maxdepth 1 -type f -name "*.md")
DIRSOURCES := $(shell cd markdown; find -mindepth 1 -maxdepth 1 -type d -exec realpath --relative-to . {} \;)
ARTDIR := public/article
ARTICLES := $(addprefix $(ARTDIR)/,$(SOURCES:.md=.html))
DIRARTICLES := $(addprefix $(ARTDIR)/,$(DIRSOURCES))

all: public/index.html public/styles.css public/favicon.ico public/article $(ARTICLES) $(DIRARTICLES)

public/favicon.ico: stubs/favicon.ico
	cp $< $@

public/styles.css: stubs/styles.css
	cp $< $@

public/index.html: $(addprefix markdown/,$(SOURCES)) $(addprefix markdown/,$(DIRSOURCES))
	mkdir -p public
	./scripts/index.py $^ > $@
	

public/article: public
	mkdir -p $(ARTDIR)

public: 
	mkdir -p public 
	cp stubs/styles.css public/

public/article/%.html: markdown/%.md
	./scripts/frontdown -f footnotes,strikethrough,fencedcode < $< | ./scripts/glue.sh /dev/stdin > $@

public/article/%: markdown/%/ markdown/%/Makefile
	cd $< && make all
	mkdir -p $@
	cp -Rt $@ $<build/*

%PHONY: clean public

clean:
	rm -rf public
	echo $(addprefix markdown/,$(DIRSOURCES)) | xargs -n 1 make clean -C
