#!/bin/bash

reldir=`dirname $0`

if [ -z $1 ];
then
	>&2 echo "first argument should be the input markdown file"
	exit 1
fi

echo "<html>"
echo "<head>"
echo '<title>Bonsaibeef</title>'
echo '<meta charset="utf-8">'
echo '<meta name="viewport" content="width=device-width, initial-scale=1.0">'
echo '<link rel="stylesheet" type="text/css" href="/styles.css" />' 
echo '<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />'
echo "</head>"
echo '<body class="wrapper">'
cat ${reldir}/../stubs/nav.html
echo "<main>"
cat $1
echo "</main>"
cat ${reldir}/../stubs/footer.html
echo "</body>"
echo "</html>"
