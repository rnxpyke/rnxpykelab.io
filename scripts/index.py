#!/usr/bin/env python3

import yaml
import frontmatter
import sys
import re
import os
from datetime import datetime

# <div class=post>
# <p class=title>$Title$</p>
# <p class=author>$Author$</p>
# <
# </div>
#
#

# (markdown)/(=name.*)(.md)
# /article/(name).html

def header():
    nav = open("stubs/nav.html", "r").read()
    return(f"""
<html>
    <head>
        <title>Bonsaibeef</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="styles.css" />
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    </head>
    <body class="wrapper">
        {nav}
""")


def footer():
    return("""
    </body>
</html>
""");

def htmlize(path, post):
    published = datetime.strftime(post['published'], '%Y-%m-%d')

    if os.path.isdir(path):
        pattern = r"^markdown/(.*)"
        match = re.match(pattern, path)
        if match:
            path = f'article/{match.group(1)}/'
        else:
            raise ValueError(f"the post path does no tmatch the '{pattern}' regex")
    else:
        pattern = r"^markdown/(.*).md$"
        match = re.match(pattern, path)
        if match:
            path =  f'article/{match.group(1)}.html'
        else:
            raise ValueError(f"the post path does not match the '{pattern}' regex")
    return (f"""
<div class=post>
    <a href=\"{path}\" class=title>{post['title']}</a>
    <p class=author>{post['author']}</p>
    <p class=date>{published}</p>
</div>
    """)

def main():
    files, dirs = ([],[])
    for path in sys.argv[1:]:
        if os.path.isfile(path):
            files.append(path)
        if os.path.isdir(path):
            dirs.append(path)
    
    filedata = { file: frontmatter.load(file).metadata for file in files }
    dirdata = { path: yaml.safe_load(open(path + '/Metadata.yaml')) for path in dirs} 
    
    data = filedata | dirdata
    print(data, file=sys.stderr)
    data = { k: v for k,v in data.items() if 'published' in v }

    print(header())
   
    for path, meta in sorted(data.items(), key=lambda item: item[1]['published'], reverse=True):
        print(htmlize(path,meta))

    print(footer())

if __name__ == "__main__":
    main()
